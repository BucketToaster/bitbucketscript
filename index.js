if(process.argv.length<5){
    throw "You have to provide 3 arguments, package name, package version and repository name.";
}  
const packageName = process.argv[2];
const packageVersion = process.argv[3];
const repo = process.argv[4];

const { v4: uuidv4 } = require('uuid');
const { Bitbucket } = require('bitbucket');
const fs = require('fs');
const simpleGit = require('simple-git');

const dir = repo; // Physical directory name would be the same as bitbucket, as we simply clone it.

const options = {
    baseDir: process.cwd(),
}
const git = simpleGit(options);

const clientOptions = {  // Credentials shouldn't be passed like this.
    auth: {
      username: 'BucketToaster',
      password: 'asd@@##22Pass',
    },
}
const repoPath = `https://BucketToaster@bitbucket.org/BucketToaster/${dir}.git`; // as userName is hardcoded as well, this could be improved
const bitbucket = new Bitbucket(clientOptions)

// remove directory if it exists in the script folder, to clone a new the most up to date version
removeDirectorySync(dir);

// clone the project, add a new branch with edited package.json, push this and open the PR.
// This block of code could be optimized as well, we can create separate function that updates file and encapsulate some logic here.
git.clone(repoPath) 
.then(() => {
    console.log('The new version was successfully cloned.')
    git.cwd(dir) // we have cloned, now we go into the cloned directory
    .then(() => { 
        const branchName = uuidv4();
        git.raw('checkout','-b', branchName) // create a branch with random uuid name
        .then(() => {
            const packageJson = JSON.parse(fs.readFileSync(`${dir}/package.json`, 'utf8')); // Here could be some errors, i.e. if no such file
            packageJson.dependencies[packageName] = packageVersion;
            console.log('New package Json:', packageJson);
            fs.writeFileSync(`${dir}/package.json`, JSON.stringify(packageJson, null, '\t'));
            git.raw('add', '.')
            .then(() => {
                git.raw('commit', '-m', `'created new branch'`)
                .then(() => {
                    git.raw('push', '--set-upstream', 'origin', branchName)
                    .then(() => {
                        createPullRequest(repo, branchName, bitbucket);
                    })
                    .catch((err) => console.log(err))
                })
                .catch((err) => console.log(err))
            })
        })
        .catch((err)=> console.log(err))

    })
    .catch((err)=> console.log(err))
})
.catch((err) => console.log(err));

// remove dir again, to clean up
removeDirectorySync(dir);


function createPullRequest(repo, branchName, bitbucket){
    const repo_slug = repo
    const workspace = 'BucketToaster'  // could be passed as function arg as well, now it's hardcoded to bucketToaster
  
    const _body = {
        title: `package JSON auto generated Pull Request ${branchName}`,
        source: {
          branch: {
              name: branchName
          }
      }
    }
  
    bitbucket.pullrequests.create({ _body, repo_slug, workspace })
    .then(() => {
        console.log('The Pull Request was successfully created')
    })
    .catch((err) => {console.log(err)})
}

function removeDirectorySync(dir){
    try {
        fs.rmdirSync(dir, { recursive: true });
    } catch (err) {
        console.error(`Error while deleting ${dir}.`);
    }
    
}